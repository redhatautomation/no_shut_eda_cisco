# no_shut_eda_cisco

Files for the `no shut ports` eda demo

## Getting started

The following links will explain how to install EDA and the other components used in the demo.

### Download the software (Skip this step if you already have AAP + EDA installed)
The EDA Controller GUI is part of a normal AAP install.
1. Option-1: Try AAP for free (60 days): https://www.redhat.com/en/technologies/management/ansible/try-it

Download Page: https://access.redhat.com/downloads/content/480/ver=2.1/rhel---8/2.1/x86_64/product-software

2. Option-2: Free Developer License: https://developers.redhat.com/products/ansible/overview
Note, this license is permenant but has limited endpoints.

### Install AAP with the automation controller and the EDA controller GUI

1. AAP installation guide
https://docs.redhat.com/en/documentation/red_hat_ansible_automation_platform/2.4/html/red_hat_ansible_automation_platform_installation_guide/index

2. Automation Controller to EDA Controller integration

⌨️ Create an Automation Controller token

#### Configuring the integration
Event-Driven Ansible is able to act in response to conditions within your environment. These actions range from debugging an event to taking direct action by running a job template in Automation Controller. To make this possible, Event-Driven Ansible Controller must first have access to your existing automation catalog. For this, we need to create a token for a user that has the proper access to your Automation Controller.

##### Go to the Automation Controller tab and login with the your credentials.

In the left-hand side menu, select users and open the link for the "admin" user.
Select the "Tokens" tab and then the blue "Add" 
![token](images/token.png)

Leave the field for "Application" blank
For "Description", specify "EDA"
For "Scope" select "write" from the drop-down menu
Click "Save"
Press the copy button to the right of the token to copy the token to your clipboard

![copy](images/copy.png)

##### EDA Controller
Select the browswer tab for "EDA Controller'' and login with the your credentials:
Select the "Admin" username in the top right corner and click "User details"

![userdetails](images/userdetails.png)

Click "Controller Tokens", "Create controller token"
For "Name" type "Controller"
Enter any description you like
Paste the token you copied from Automation Controller into the "Token" field
You are able to launch a job template from the Automation Controller you have access to in this environment.

## Router - Streaming Telemetry
The configuration for the router's telemery is available here:
https://gitlab.com/redhatautomation/no_shut_eda_cisco/-/blob/main/rtr_telegraf.cfg?ref_type=heads


## Other Services Needed for the EDA Demo
You can install these servcies on your lab automation controller vm or somewhere else. Also podman container options can work as well. 

### Telegraf Collector Install and Configuration
1. Install https://docs.influxdata.com/telegraf/v1.21/introduction/installation/


2. copy the config file and replace the one generated at the /etc/telegraf/telegraf.conf location

Please see this repo for my example telegraf.conf file.

3. systemd
~~~ 
sudo systemctl start telegraf
~~~

### Kafka Install
1. Install https://kafka.apache.org/quickstart

2. Scripts to run kafka. Change directories to your kafka directory from the install

zookeeper
~~~
cd kafka_2.13-3.5.0/
./bin/zookeeper-server-start.sh config/zookeeper.properties
~~~
Create a kafka topic
~~~
cd kafka_2.13-3.5.0
bin/kafka-topics.sh --create --topic telegraf --bootstrap-server localhost:9092
~~~
Run Kafka
~~~
cd kafka_2.13-3.5.0
./bin/kafka-server-start.sh config/server.properties
~~~
Run te Kafka viewer
~~~
cd kafka_2.13-3.5.0
bin/kafka-console-consumer.sh --topic telegraf  --bootstrap-server  ip-172-16-213-105.us-east-2.compute.internal:9092
~~~

## Background for Event Driven Ansible
ansible-rulebook relies on a few components. These components all come together to allow you to respond to events and decide on the next course of action. The three main components to the rulebooks are as follows:

* Source - The sources of the events come from source plugins. These plugins define what    Event-Driven Ansible is listening to for events.
* Condition - The conditional statements in the rulebook allow us to match criteria on which one want to have some kind of response to.
* Action - Lastly, the action is the response once the condition has been met from the event source. This can be to trigger remediation, log a ticket for observation, or generate other events which would need responding to.

Currently, there are a number of source plugins however this list is being developed and as more partners get involved in the project the list will expand.
![rulebook](images/rulebook.png)

![table](images/table.png)

## Demo Rulebook
The rule book tells us that we are listening to a Kafka topic.
`kafka-iterface-rules.yml`

The source plugin is set to kafka on tcp port 9092
~~~
- name: Rules with kafka as source
  hosts: localhost
  sources:
    - ansible.eda.kafka:
        topic: telegraf
        host: ip-172-16-213-105.us-east-2.compute.internal
        port: 9092
        group_id:
~~~
The rule is watching for protected ports that go down when they are defined in a list called protected
~~~
  rules:
    - name: Retrieve Data and Launch Job-template
      condition: events.body.fields.new_state == "interface-notif-state-down" and events.body.fields.if_name in vars.protected
~~~
When the rule contdition is met it will trigger an action in the EDA controller to run a playbook job to `no-shut` the affectd port by triggering the job-template "EDA-FIX-PORTS". Extra variables are also passed to the playbook to define the host_name of the router and the affected port/interface.
~~~
      action:
        run_job_template:
          name: "EDA-Fix-Ports"
          organization: "Default"
          job_args:
            extra_vars:
              interface: "{{ event.body.fields.if_name }}"
              rtr: "{{ event.body.fields.host_name }}"
~~~

## EDA Controller

### Configure a project to connect to the Gitlab Repo
You can point it to this public repo. Remember to sync the repo afterwards
![edaproject](images/edaproject.png)

### Configure a rulebook activation
Here is where you select the `kafka-interface-rules.yml` rulebook. Notice at the bottom of the config is where you define the list `protected` for the interfaces. Lastly, you must activate the rulebook. You should see it running
![rulebookactivation](images/rulebookactivation.png)

## Playbook
`noshut.yml`
The no shut playbook is straightforward. Basically it reads in the extra variables passed from EDA and using them to no shot the appropriate router's interface and validate the interface is now functioning.

~~~
---
- name: Triage Down Interfaces 
  hosts: "{{ rtr }}"
  gather_facts: False
  tasks:
  - name: Attempt a No shut for {{ inventory_hostname }} {{ interface }}
    cisco.ios.ios_interfaces:
      config:
      - name: "{{ interface }}"
        enabled: True

  - name: Check Interface State for {{ inventory_hostname }} {{ interface }}
    cisco.ios.command:
      commands: show ip interface brief
    register: int_state

  - name: Print Event Info
    ansible.builtin.debug:
      var: int_state.stdout_lines[0]
~~~

## Automation Controller
To run the playbook we must configure all of the normal contructs for AAP automation:
* Inventory
* Project
* Job-Template

Inventory:
Here is an example invnetory group for Cisco ios devices
![group](images/group.png)

Project: 
This will point to `https://gitlab.com/redhatautomation/no_shut_eda_cisco.git`

![project](images/project.png)

Job-Template:
The `EDA-Fix-Ports` job-template manages the noshut.yml playbook. Please note it's required to select prompt for extra varaibles to allow EDA to pass variables to the playbook. Lastly, credentials to access your routers must be defined.

![job-template](images/job-template.png)

## Running the Demo
Congratulations for configuring this demo. Please refer to this demo video to learn how to run the demo

https://www.youtube.com/watch?v=jBn2-lTHjEQ&lc=UgwDkWfZ2Juu3GlgpsB4AaABAg.A6YMPru8OIyA6c5iom3Hsa
